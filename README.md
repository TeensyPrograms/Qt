This Blink program was used as a test to setup Qt to be used as an editor for
Teensy programs.  I use Qt for other work so it's familiar to me and I just
didn't want to use the Arduino IDE.  

Notes:

    1.  This is based on others work - see the References below
    2.  Currently this only handles Teensy 3.2.  I plan on changing this so that 
        the parameters can be entered and the Makefile created with them.
    3.  The main.cpp is a sample from one of Paul's repositories.
    4.  Tools, libraries, and other files were copied from the Arduino
        directory after the Teensyduino was installed.  Later they will be moved 
        to a directory for use by all projects.

To-Do:

    1.  Create automated Makefile generation in which the user fills in values
        for the needed variables
    2.  See if a Qt project other then import can be used to create a Teensy
        project.  The advantage of this approach is that the project creation 
        wizard can be used get the values needed and create a Makefile with 
        them all setup.
    3.  Change MakeFile to enter the build directory and not set it up in Qt.
        
Setup:

The BlinkLED project directory has the following structure.  My main directory
for Teensy projects is D:/Projects/TeensyProjects and BlinkLED was put in the 
BlinkLED directory under TeensyProjects.

BlinkLED - main, home project directory
    Build - used to build the project.  the .hex, .0, and other build files end
        up here
    *libraries - contains the Teensy libraries.  Not used yet
    src - Put the *.c and *.cpp, files in here.  main.cpp resides here.
    *teensy3 - contains the files from 
        <installlocation>\Arduino\hardware\teensy\avr\cores\teensy3
    *tools - the teensy tools
    
Directories marked with a * will be moved to a TeensyCommon directory so they
don not have to be included in every project but to keep things simple during 
testing they were put in the BlinkLED directory.

Installation - Files

First the files have to be installed to a directory of your choice.

    1.  Download the BlinkLED directory to your computer.
    2.  Open Makefile in a text editor.
        * The User Defined Variables section is between the comment lines 
          consisting of the = sign are variables that need to be set for your
          installation.  Nothing after them should need to be set as everything
          else is dervived from them.
    3.  Save the file and go to the Installation - Qt
    
Installation - Qt

Qtcreator is the IDE for the Qt development environment and is used as an editor
for the projects.  If you do not use Qt simply do not do this part of the
installation.

This project is imported into Qt using New Projects -> Import Project ->
Import Existing Project.  If anything else is used Qt will use qmake which
will not work for this project because it assumes you want to use Qt libraries 
and creates it's own Makefile which doesn't work on this project.  

    1.  Open Qtcreator and from File -> New File or Project select 
        Import Project -> Import Existing Project
    2.  Fill in the information.
        *   Enter a title
        *   Browse to the directory containing your project and select it. Click
            "Next".
        *   The File selection dialg allows selection of what files will be
            shown and available in the IDE.  You can always add files later.
            On the File Selection change the "Hide files matching: filter by 
            removing Makefile*; and applying the filter
        *   In the File Selction the only things that should be checked are the
            following.  The remaining files are referenced during make but don't
            need to be available in the IDE but can be show if desired.
                src
                Makefile
                .gitignore (only if using git)
            
            Check and then uncheck teensy3 and tools.  This makes sure that no
            files under these directories are selected.
        *   If desired setup source control, otherwise continue.  Qtcreator will
            open the project.
    3.  Set the project Build settings.  At this point Qt has some default
        settings the need to be modified.  I created three configurations so I
        could make the project, clean it, and upload it by choosing the
        desired configuration from the build sidebar.
            *   Select Projects in the sidebar and the Build & Run tab.
            *   There will be a default show in the Edit build configuration
                dropdown.
            *   Click the dropdown labeled Add and select "Clone Selected"
            *   Enter the name all and the Edit build configuration will now
                show all
            *   Edit the settings
                *   Set the Build directory to <yourinstall>\BlinkLED\Build
                *   Under Build Steps show Details.
                    *   If desired you can use another make but Qt's make works
                        just fine.
                    *   Into Make arguments enter this.  The build process starts
                        in the directory entered above so this tells Qt to
                        run make with the file in the project directory.
                        -f %{CurrentProject:Path}/Makefile
                    *   Under Targets all should be the only one checked.
                    *   Delete any Clean Steps
            *   With all selected clone it and name it clean. The Edit build 
                configuration will now show clean
                *   Edit the settings.  Only the Target needs to be changed
                    *   In the Build Steps change the target to clean and 
                        uncheck all.
                    *   Delete any Clean Steps
            *   With all selected clone it and name it upload.  The Edit build
                configuration will now show upload.
                *   Edit the settings. 
                    *   Into Make arguments enter this.  This adds the rule, 
                        upload, to be executed.
                            -f %{CurrentProject:Path}/Makefile upload
                    *   Uncheck both all and clean Targets
                    *   Delete any Clean Steps.

From Qtcreator there are three choices for configurations - all, clean, upload.
Select the desired one and build the project (this project does not run anything
so don't use run).  If everything is set correctly running build will execute
the configuration chosen.

    all - builds the hex file (but does not upload it)
    upload - uploads the hex file to the Teensy MCU
    clean - cleans up files used to build

                    
                    
        




    
